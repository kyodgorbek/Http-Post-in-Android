# Http-Post-in-Android
InputStream is = this.getAssets().open("data.xml");
HttpClient httpClient = new DefaultHttpClient();
HttpPost postRequest = new HttpPost("http://192.178.10.131./WS2/Upload.aspx");

byte[data] = IOUtils.toByteArray(is);

InputStreamBody isb = new InputStreamBody(
                         new ByteArrayInputStream(data),"uploadedFile");
StringBody sb1 = new StringBody("someTextGoesHere");
StringBody sb2 = new String Body("someTextGoesHere too");

MultiplepartEntity multipartContent = new MultipartEntity();
multipartContent.addPart("uploadedFile", isb);
multipartContent.addPart("one", sb1);
multipartContent.addPart("two", sb2);

postRequest.setEntity(multipartContent);
HttpResponse res =httpClient.execute(postRequest);
res.getEntity().getContent().close();
